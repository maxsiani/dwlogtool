let utils = {};

utils.pad = function(string, totalSize, pad) {
	pad = pad === null ? ' ' : pad;
	return ('' + pad + string).slice(-totalSize);
};


module.exports = utils;
