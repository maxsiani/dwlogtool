const path = require('path');

const DwLoader = require('./DwLoader');
const packageJson = require('./package.json');
const utils = require(__dirname + '/utils.js');

print(`\nDW log tool - version ${packageJson.version}\n\n`);


// default values (can be overwritten in log.conf.js if exist) ////////////////////////////////////////////////////////////////////////////////////////////////////////
host = "your-sandbox.demandware.net";
user = "your-user";
pass = "your-pass";

bytes = 2048; // initial latest bytes to show from the log
seconds = 3; // number of seconds to sleep between change requests to the server
subfolder = ""; // case sensitive, empty OR ( begin without "/" AND end with "/" - example: "deprecation/" )
ua = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11"; // Chrome UserAgent, necessary for some instances
use_color = true; // ANSI colors

profile = [];
try {
  require(__dirname + '/log.conf.js');
} catch(e) {
	let dwJsonPath = path.join(__dirname, '..', '..', 'cartridges', 'dw.json');
	console.log(`Loading the file dw.json from ${dwJsonPath}`);
	const dwProfile = DwLoader.loadProfileFromDwJson(dwJsonPath);
	profile = Object.assign({}, dwProfile);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var https = require('https');

var all_logs = [];
var all_sizes = [];
var all_dates = [];

var latest_logs = [];
var latest_logs_sizes = [];
var latest_logs_dates = [];

var size = 0;
var log_num = -1;

var now_date = new Date();
now_date = utils.pad(now_date.getFullYear(), 4) + utils.pad(now_date.getMonth() + 1, 2, '0') + utils.pad(now_date.getDate(), 2, '0');

// enable user input, so you can hit Enters
var stdin = process.stdin;
if (stdin.setRawMode) {
	stdin.setRawMode(true); // without this, we would only get streams once enter is pressed
}
stdin.setEncoding('utf8'); // I don't want binary, do you?
stdin.resume();

var Step1_Profiles = function() {
	if (!process.argv[2]){
		print("\nPROFILES:\n\n\n");
		for (var p in profile) {
			print("  [" + str_color(p, "green") + "] " + profile[p]['user'] + "@" + profile[p]['host'] +
				(profile[p]['subfolder']? "/" + profile[p]['subfolder']: "") +
				( profile[p]['seconds'] || profile[p]['bytes'] || profile[p]['ua'] || profile[p]['use_color']?
					" [" +
						(profile[p]['seconds']? profile[p]['seconds'] + " sec; ": "") +
						(profile[p]['bytes']? profile[p]['bytes'] + " bytes; ": "") +
						(typeof profile[p]['ua'] != 'undefined'? "UA; ": "") +
						(typeof profile[p]['use_color'] != 'undefined'? (profile[p]['use_color']? 'color; ': "no color; "): "") +
					"]": ""
				) + "\n");
		}
		stdin.pause();
		return;
	}

	if (!profile[process.argv[2]]){
		die("\nERROR: Specified profile not found.\n");
	}
	for (var p in profile[process.argv[2]]) {
		global[p] = profile[process.argv[2]][p];
	}
	Step2_ListLogs();
};

var Step2_ListLogs = function() {
	var options = {
		rejectUnauthorized: false,
		auth: user + ':' + pass,
		headers: { 'User-Agent': ua },
		hostname: host,
		port: 443,
		path: '/on/demandware.servlet/webdav/Sites/Logs/' + subfolder,
		method: 'GET'
	};
	doRequest(options, function(data) {
		var regexp = new RegExp('<a href="/on/demandware\.servlet/webdav/Sites/Logs/' + subfolder + '(.*?)">[\\s\\S]*?<td align="right"><tt>(.*?)</tt></td>[\\s\\S]*?<td align="right"><tt>(.*?)</tt></td>', 'gim');
		var match = regexp.exec(data);

		while (match != null) {
			all_logs.push(match[1]);
			all_sizes.push(match[2]);
			all_dates.push(match[3]);
			match = regexp.exec(data);
		}

		var latest_logs_tmp = [];
		var latest_logs_tmp_sizes = [];
		var latest_logs_tmp_dates = [];

		for (i in all_logs) {
			var log = all_logs[i];
			if (log.substr(-4) != '.log') {
				continue;
			}

			// get only logs with dates as suffix
			if (isNaN( parseInt(log.substr(-12, 8)) ) || parseInt(log.substr(-12, 8)) < 10000000) {
				continue;
			}

			// get the latest logs only
			if (parseInt(latest_logs_tmp[ log.slice(0, -12) ]) > parseInt( log.substr(-12, 8) )) {
				continue;
			}

			latest_logs_tmp[ log.slice(0, -12) ] = parseInt( log.substr(-12, 8) );
			latest_logs_tmp_sizes[ log.slice(0, -12) ] = all_sizes[i];
			latest_logs_tmp_dates[ log.slice(0, -12) ] = all_dates[i];
		}

		// only the latest dates are picked
		for (i in latest_logs_tmp) {
			latest_logs.push( i + latest_logs_tmp[i] + '.log' );
			latest_logs_sizes.push( latest_logs_tmp_sizes[i] );
			latest_logs_dates.push( latest_logs_tmp_dates[i] );
		}

		// log files without dates are added at the end of the generated list
		for (i in all_logs) {
			var log = all_logs[i];

			// no directories
			if (log.substr(-4) != '.log') {
				continue;
			}

			// get only logs without dates as suffix
			if (!isNaN( parseInt(log.substr(-12, 8)) ) && parseInt( log.substr(-12, 8) ) > 10000000) {
				continue;
			}

			latest_logs.push( log );
			latest_logs_sizes.push( all_sizes[i] );
			latest_logs_dates.push( all_dates[i] );
		}

		if (!process.argv[3]) {
			print("\nLATEST LOGS on [" + str_color(process.argv[2], "green") + "] " + user + "@" + host +
				(subfolder? "/" + subfolder: "") +
				" [" + seconds + " sec; " + bytes + " bytes; " +
					(typeof profile[process.argv[2]]['ua'] != 'undefined'? "UA; ": "") +
					(typeof profile[process.argv[2]]['use_color'] != 'undefined'? (profile[process.argv[2]]['use_color']? 'color; ': "no color; "): "") +
				"]\n\n\n");
			for (i in latest_logs) {
				var log_date = new Date(latest_logs_dates[i]);
				log_date = '' + log_date.getFullYear() + (log_date.getMonth() < 9? '0': '') + (log_date.getMonth() + 1) + (log_date.getDate() < 10? '0': '') + log_date.getDate();
				var is_today = now_date === log_date;

				print( "  [" + str_color(i, "green") + "] " +
					latest_logs[i] + " [" +
					str_color(latest_logs_sizes[i], is_today && latest_logs_sizes[i] !== '0.0 kb'? "brown": false) + "; " + // if Today and Size != 0, color the Size brown
					str_color(latest_logs_dates[i], is_today? "cyan": false) + "]\n"); // if Today, color it cyan
			}
			stdin.pause();
			return;
		}

		if (process.argv.length > 4 && process.argv[4] === 'download') {
			log_num = parseInt( process.argv[3] );
			var options = {
				rejectUnauthorized: false,
				auth: user + ':' + pass,
				headers: { 'User-Agent': ua },
				hostname: host,
				port: 443,
				path: '/on/demandware.servlet/webdav/Sites/Logs/' + subfolder + latest_logs[log_num],
				method: 'GET'
			};

			doRequest(options, function (data, headers, code) {
				if ([200, 206].indexOf(code) > -1) {
					print_custom_file(data, latest_logs[log_num]);
				}
				stdin.pause();
			}, false);

			return;
		}

		Step3_ShowLog();

	}); // doRequest
};

var Step3_ShowLog = function() {
	if (!latest_logs[process.argv[3]]) {
		die("\nERROR: Log file number not found.\n");
	}

	log_num = parseInt( process.argv[3] );
	var log_date = new Date(latest_logs_dates[log_num]);
	log_date = '' + log_date.getFullYear() + (log_date.getMonth() < 9? '0': '') + (log_date.getMonth() + 1) + (log_date.getDate() < 10? '0': '') + log_date.getDate();
	var is_today = now_date === log_date;

	print( str_color("\nPROFILE: ", "brown") + "[" + str_color(process.argv[2], "green") + "] " + user + "@" + host + "/" + subfolder + " [" + seconds + " seconds; " + bytes + " bytes]" );
	print( str_color("\nLOG: ", "brown") + "[" + str_color(log_num, "green") + "] " + latest_logs[log_num] + " [" +
		str_color(latest_logs_sizes[log_num], is_today && latest_logs_sizes[log_num] !== '0.0 kb'? "brown": false) + "; " + // if Today and Size != 0, color the Size brown
		str_color(latest_logs_dates[log_num], is_today? "cyan": false) + "]" ); // if Today, color it cyan

	// get only the headers (HEAD request) - to get only the Content-Length
	var options = {
		rejectUnauthorized: false,
		auth: user + ':' + pass,
		headers: { 'User-Agent': ua },
		hostname: host,
		port: 443,
		path: '/on/demandware.servlet/webdav/Sites/Logs/' + subfolder + latest_logs[log_num],
		method: 'HEAD'
	};

	stdin.on('data', function(key) {
		//console.log('key: ' + JSON.stringify(key));
		switch (key) {
			case 'h': case 'H': case '?':
			case 'h\r\n': case 'H\r\n': case '?\r\n': // in case the setRawMode is not defined (eclipse)
			case 'h\r': case 'H\r': case '?\r':
			case 'h\n': case 'H\n': case '?\n':
			case 'h\n\r': case 'H\n\r': case '?\n\r': // mac ?
				print('\n\nAllowed keys:\n  h,H,?: this message\n  \\n,\\r: new line\n  space: 25 new lines\n  q,Q,ctrl+c: quit\n\n');
				break;

			case '\n': case '\r': case '\n\r': case '\r\n':
				print('\n');
				break;

			case ' ': case ' \r': case ' \n\r': case ' \r\n':
				print('\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n');
				break;

			// 3: ctrl+c
			case '\u0003': case 'q': case 'Q':
			case '\u0003\r\n': case 'q\r\n': case 'Q\r\n':
			case '\u0003\r': case 'q\r': case 'Q\r':
			case '\u0003\n': case 'q\n': case 'Q\n':
			case '\u0003\n\r': case 'q\n\r': case 'Q\n\r':
				process.exit();
				break;
		}
	});

	doRequest(options, function(data, headers) {
		size = headers && headers['content-length']? +headers['content-length']: 0;

		print( str_color("\nRETURNED SIZE: ", "brown") + size + " bytes\n\n\n" );

		// first request will not get the whole file, but only "bytes" from it's end
		size = size - bytes > 0? size - bytes: 0;

		setTimeout(Step3_ShowLogTimer, 0);
	}); // doRequest
};

var Step3_ShowLogTimer = function() {
	var options = {
		rejectUnauthorized: false,
		auth: user + ':' + pass,
		headers: { 'User-Agent': ua, 'Range': 'bytes=' + size + '-'},
		hostname: host,
		port: 443,
		path: '/on/demandware.servlet/webdav/Sites/Logs/' + subfolder + latest_logs[log_num],
		method: 'GET'
	};
	doRequest(options, function(data, headers, code) {
		//console.log(options, headers, code);
		var diff_size = 0;
		diff_size = parseInt( headers && headers['content-length'] );

		// 206 Partial Content
		// 416 Requested Range Not Satisfiable
		if (code == 206) {
			size += diff_size;
			print_custom( data );
		}

		setTimeout(Step3_ShowLogTimer, seconds * 1000);
	}, true); // doRequest with silence
};

function doRequest(options, callback, silence) {
	var data = '';
	var req = https.request(options, function(res) {
		res.on('data', function(d) {
			data += d;
		});
		res.on('error', function(e) {
			if (!silence) {
				console.log(e);
				console.log(options);
				die("\nERROR: Connection failed.\n");
			} else {
				if (callback) {
					callback('', {}, 416);
				}
			}
		});
		res.on('end', function() {
			if (callback) {
				callback(data, res.headers, res.statusCode);
			}
		});
	});
	req.on('error', function(e) {
		if (!silence) {
			console.log(e);
			console.log(options);
			die("\nERROR: Connection failed.\n");
		} else {
			if (callback) {
				callback('', {}, 416);
			}
		}

	});
	req.end();
}

// returns a string with color
function str_color(str, color) {
	color = typeof color !== 'undefined'? color: false;

	if (!use_color || !color) {
		return str;
	} else {
		var colors = {'red': 31, "green": 32, "brown": 33, "blue": 34, "purple": 35, "cyan": 36, 'lightgray': 37};
		var c = colors[color]? colors[color]: colors["lightgray"];
		return "\033[" + c + "m" + str + "\033[0m";
	}
}

// print custom data
function print_custom(data) {
	var data = data.split('\n');
	for (i in data) {
		if (data[i][0] == '[' && data[i][28] == ']') {
			data[i] = str_color(data[i], 'brown') + '\n\n';
		} else if (data[i].trim() == '"') {
			data[i] = '\n\n';
		}
	}
	print(data.join('\n'));
}

function print_custom_file(data, filename) {
	let writtenData = data.split('\n').map(function(line) {
		if (data[i].trim() === '"') {
			line = '\n';
		}
		return line;
	}).join('\n');

	let fs = require('fs');
	fs.appendFile(filename, writtenData, err => {
		if (err) {
			return console.log(err);
		}
	});
}

function print(s) { process.stdout.write(s); }
function die(s) { print(str_color(s, "red")); process.exit(0); }

Step1_Profiles();
