'use strict';

module.exports = class DwLoader {

    /**
     * @param {string} path
     */
    static loadProfileFromDwJson(path) {
        try {
            /** @type {DwJson} */
            const dwJson = require(path);
            const name = dwJson.profile || dwJson.hostname.split('-')[0].split('-')[0];
            let profiles = {};
            profiles[name] = ProfileFactory.createProfile(dwJson.hostname, dwJson.username, dwJson.password);
            return profiles;
        } catch (e) {
            console.log(e.message);
            return {};
        }
    }
}

class ProfileFactory {
    static createProfile(host, user, pass) {
        return {
            host,
            user,
            pass
        };
    }
}
