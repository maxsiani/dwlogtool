# Demandware Log Tool

> With this tool you can "tail -f" the demandware logs.

> It is sending HEAD requests on predefined intervals to check for a file size change.

> On change it sends a GET request with a "range" header, to get only the delta bytes of the log file.


## Features

- Profiles are in a separate file (*log.conf.js*)
- Interactive key bindings (space - 25 new lines, enter - new line, h - help message)
- Coloring the log files listing (non-zero size, from today)
- Listing only the latest log files
- Profiles configuration include:
    * Change user-agent
    * Polling interval seconds
    * Initial number of bytes to get from the end of the log file

## Requirements
- nodeJS

## Installation

1. Get locally **log.conf-sample.js** and **log.js**
2. Rename **log.conf-sample.js** to **log.conf.js**
3. Edit **log.conf.js** with your settings


## Usage

- `node log` to list profiles
- `node log dev01` to list the latest log files on the selected profile (dev01) instance
- `node log dev01 8` to tail the specified (8) log file on the instance

## Profile configuration options in *log.conf.js*

- `host`: instance hostname
- `user` and `pass`: credentials to log into your instance
- `bytes`: initial number of bytes to get from the end of the log file
- `seconds`: polling interval seconds
- `subfolder`: subfolder of the log files, example: "deprecation/"
- `ua`: user-agent string to send with requests
- `use_color`: turn on/off ansi coloring
